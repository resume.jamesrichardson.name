
(require 'ox-html)
(setq this-directory (file-name-directory (or load-file-name buffer-file-name)))
;;(setq pub-directory "/ssh:resume.jamestechnotes.com:/home/public/")
(setq pub-directory "~/public_html/resume.jamestechnotes.com")
(setq org-publish-timestamp-directory (concat (or (when (boundp 'temporary-file-directory) temporary-file-directory)
                                                  "/tmp/")
                                              ".org-timestamps/"))
(setq org-html-htmlize-output-type 'css)

(unless (boundp 'org-publish-project-alist)
  (setq org-publish-project-alist nil))

(add-to-list
 'org-publish-project-alist
 `("resume-content"
   :base-directory ,this-directory
   :base-extension "org"
   :publishing-directory ,(expand-file-name pub-directory)
   :publishing-function (org-html-publish-to-html org-latex-publish-to-pdf)
   :recursvie t
   :headline-levels 6
   :auto-preamble nil
   :auto-sitemap nil
   :makeindex nil
   :html-head-include-default-style nil
   :html-head-include-scripts nil
   :html-head-extra "<link rel=\"stylesheet\" title=\"Default\" href=\"/css/stylesheet.css\" type=\"text/css\" />
  <script src=\"/js/org.js\"></script>
  <link rel=author href=\"https://plus.google.com/u/0/100087072360376597478?rel=author\" />"
   :completion-function (jr-resume-content-rename-resume-to-index)
   ))

(add-to-list
 'org-publish-project-alist
 '("resume" :components ("resume-content")))

(defun jr-resume-content-rename-resume-to-index ()
  "Rename resume.html to index.html after publish."
  (let* ((pub-dir (file-name-as-directory (plist-get project-plist :publishing-directory))))
    (princ (file-name-as-directory (plist-get project-plist :publishing-directory)))
    (princ project-plist)
    (princ "james")
    (let ((default-directory pub-dir))
      (rename-file (expand-file-name "resume.html" pub-dir) (expand-file-name "index.html") t))))
