#+OPTIONS: toc:nil title:crap author:nil

#+BEGIN_CENTER
*James M. Richardson, Jr.* \\
james@jamestechnotes.com\\   
704-380-0239
#+END_CENTER

_*Professional Summary:*_
+ *20+ years* of experience in IT across many industries including
  retail, manufacturing, telecommunications, and insurance in
  *UNIX/Linux engineering and software development roles*. Experienced
  in *installation, configuration, design, and deployment* of *Open
  SuSE*, *Red Hat*, *RHEL*, *Debian*, *AIX*, and *Solaris*. *Software
  Development* experience includes *C*, *C++*, *Perl*, *Python*, with
  various workflows utilizing *GIT*, *SVN*, *CVS*, and *build systems*
  such as *GNU Autotools* and tools to generate native *RPM*, *deb*,
  and *installp* packages.
+ Expertise in automating standard (and not so standard) Operating
  System and hardware maintenance tasks and building tools in *Shell*,
  *Perl*, *Python*, that can be utilized to effect deployment across
  geographically distributed environments.
+ Experience with creating and utilizing *CFEngine recipies* and
  *Puppet manifests* to automate many system administration and
  software deployment tasks.
+ Proficient in *(Distributed) Version Control Systems* such as *GIT*,
  *SVN (subversion)*, *CVS*, and *Hg (mercurial)* and merging updates
  from other engineers/developers into the HEAD to create releases.
+ Experience with designing and maintaining *bare metal installation
  services* such as *PXE*, *FAI (fully automated install)*, *preseed*,
  *jumpstart*, and *NIM*.
+ Proficient in design, implementation, and administration of typical
  infrastructure services such as *DNS*, *DHCP*, *Backup/Recovery*,
  and *email*.
+ Understanding of *Web Services* and configuration and installation
  of *Apache* and *Nginx* and *LAMP* stacks.
+ Understanding of various *virtualization* technologies such as
  *KVM*, *VMWare*, and *containers*
+ Experience writing *PAM* modules and clients and enable applications
  to take advantage of existing user authentication and authorization.
+ Proficient with *Software Development Life Cycles* *SDLC*. 

_*Technical Skills:*_
| *Operating Systems*          | Red Hat/RHEL 6.x, 7.x, AIX 5.3, 7.1, Open SuSE, Debian, CentOS, Solaris 6, 7, 8, 9     |
| *Programming Languages*      | Shell, Bash, Ksh, Perl, Python, C/C++, SQL, sed, awk, common lisp, clojure, emacs-lisp, Flex, Bison |
| *Build Systems and Packages* | GNU Autotools, Make, RPM, DEB, Installp                                                |
| *Networking*                 | TCP/IP, DNS, DHCP, NFS, FTP, SMTP, SSH                                                 |
| *Virtualization*             | kvm, VMWare Workstation, VMWare ESX, VirtualBox, Solaris zones, Linux containers       |
| *Databases*                  | sqlite, DB/2, sybase, Oracle                                                           |
| *Web Servers*                | Apache, Nginx, Squid                                                                   |

_*Professional Experience:*_

*Lowe's Companies, Mooresville, NC* \\
*February 2011 -- Present*\\
*Unix/Linux Infrastructure Engineer*\\
_Responsibilities:_\\
+ Technical lead on project evaluation and recommending migration path
  from *AIX* to *GNU/Linux* considering *RedHat/RHEL*, *Ubuntu* or
  *CentOS* with virtualization provided by *KVM* or *VMWare*.
+ Developed automated method of creating *installp* and *RPM* packages
  utilizing *autotools*, *git*. *Perl* is the primary programming
  language. Looking at incorporating process into *SDLC* and *CI* systems.
+ Developed automated process to flash firmware/microcode in IBM
  BladeCenter Chassis and Power 7 blades. *Perl* is the primary
  language with *C* and *Python* utilities to manage deployment across
  entire chain.
+ Developed automated process to upgrade and patch *AIX*
  servers. *Ksh* is the primary language.
+ Developed *PAM* module to enable in house applications to hook into
  existing authentication infrastructure. This was written in *C*.
+ Deployed *CFengine* to each *AIX* server in chain.
+ Worked with Release Management team members to optimize release
  processes.
+ Worked with Application Development team to formalize SDLC process.
+ Designed procedures and processes to ensure servers remain PCI
  compliant and known security vulnerabilities are patched in a timely manner.

*CSC, Blythewood, SC*\\
*June 2009 - February 2011*\\
*Unix System Enginner*\\
_Responsibilities:_
+ Performed *Solaris* installs -- Physical servers and Zones
+ Participated in successful disaster recovery test at remote sites
+ Configured file systems on Hitachi SANS -- Veritas, UFS, and ZFS
+ Configured mulitpathing utilizing Veritas DMP and Hitachi Dynamic Link Manager
+ Assisted backup team with NetBackup installations and trouble shooting

*Consultant, Bishopville, SC*\\
*June 2005 -- June 2009*\\
_Responsibilities:_
Provided UNIX and GNU/Linux support to various clients as required.
+ *Information Research LLC, Palo Alto, CA*
  + Provided Debian GNU/Linux technical support, remote
    administration, server configuration and advice
+ *Barnes Enterprises, Florence, SC*
  + Implemented VPN between company locations utilizing Debian GNU/Linux servers, OpenVPN, and Shorewall
  + Implemented automated installation and management procedures for Microsoft Windows XP

*Horry Telephone Cooperative, Conway, SC*\\
*July 1999 -- June 2005*\\
*Network / Systems Administrator*\\
_Responsibilities:_
+ Automated server install and patch process utilizing custom shell scripts, *CFengine*, *puppet*, *JumpStart*, and *InstantIgnition*
+ Implemented server monitoring solution utilizing *Nagios*
+ Architected and implemented Unix server backup solution utilizing Legato
+ Deployed **Tru64**, **Solaris**, **OpenVMS** and Windows NT Server to Compaq HSG80 SAN
+ Developed interface between HTC Sybase customer order system and BellSouth's provisioning system utilizing *C*
+ Architected and implemented email system for ISP, utilizing Exim, ClamAV, Courier-imap/pop3, MySQL, SpamAssassin and others

_*Education:*_
+  *Bachelor of Science in Physics* Francis Marion University, Florence, SC
