This site contains [[!pagecount pages="page(*) and !blog and !blog/* and !recentchanges and !note and !popup and !templates and !templates/* and !ikiwiki and !ikiwiki/* and !shortcuts and !smileys" and !experience/*]] pages, of which [[!pagecount pages="blog/entry/* and !*/Discussion"]] are blog entries, and [[!pagecount pages="*/Discussion"]] are Discussion pages.

Orphans:
[[!orphans pages="page(*) and !blog and !blog/* and !recentchanges and !note and !popup and !templates and !templates/*" and !experience/*]]

Broken and Future links:
Some links are for planned or future pages ;)

[[!brokenlinks ]]

